# Kanban


## Description

Kanban App is a tool to visualise workflow. It must provide functionality to create lists representing the stages of a process and cards representing tasks of the process.


## Technology Used

This project uses Flask for backend and Vue Js for frontend.
- Backend: Flask, Flask SQLALchemy - ORM, Flask Security, matplotlib, Flask Restful - APIs, smtplib
- Frontend: Vue 2.0, Vue Router, Axios


## DB Schema Design

(To be updated)

## API Design

CRUD is implemented for User, List, Card in a cascading manner.  
An API to create graphs and serve graphs is also implemented.


## Architecture and Features

The project is divided into frontend and backend. In the backend, app.py contains configuration details and api routes, models.py has database models and apis.py has code to implement the main functionality. Static folder has the images and files generated. Inside, Frontend, project, src, App.vue initialises the Vue app. The Components folder has all the different pages of the UI.  
Following features are implemented: User Login, Main Board with Lists, List Management, Card Management, Summary Page, Export Jobs


***

# Steps to run the the project

Kanban App using Flask and Vue

Steps to run Flask server:
1. Navigate to backend folder using "cd" in a Linux shell.
2. Create a python virtual environment using the command "python3 -m venv \[Name of env\]"
3. Activate virtual environment using "source \[Name of env\]/bin/activate"
4. Run "pip install -r requirements.txt"
5. Run "flask run"

Steps to start Vue server: 
The pc must have Node.js and npm installed to run the following commands. (Please refer https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/ for installation process)
1. Navigate to frontend folder using "cd" in a Linux shell.
2. Run "npm install -g npm@6.14.8"
Note: this command changes the npm version of the system globally, to undo this run "npm install -g npm@latest"
3. Run "npm install" to install the dependencies listed in "package.json"
4. Run "npm run serve"

Paste the URL shown on the terminal onto a browser and Kanban App is ready to use!

## Yet to do
- UI Improvements
- API design doc
- Daily Reminders
- Monthly Reports

## Future Features
Following features are considered to be included in future versions:
- Home Page introducing and explaining purpose of Kanban Board
- User Guide
- Feature to add multiple boards for user, creating new "project"
- Inviting members to collaborate on a board
- Tracking source code associated with project 

## Contact
Soumya Oruganti
osmanimadhavi@gmail.com




