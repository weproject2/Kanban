import UserLogin from './components/HomePage.vue'
import KanbanBoard from './components/KanbanBoard.vue'
import AddCard from './components/AddCard.vue'
import SummaryPage from './components/SummaryPage.vue'

export default [
    {path: '/', component: UserLogin},
    {path: '/board/', component: KanbanBoard},
    {path: '/card/', component: AddCard},
    {path: '/summary/', component: SummaryPage}
]