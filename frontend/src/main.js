import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes.js'
import VueSessionStorage from "vue-sessionstorage";
import vuetify from 'vuetify'
import Vuesax from 'vuesax'
import 'material-icons/iconfont/material-icons.css';
import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax, {
  // options here
})

Vue.use(VueSessionStorage);
Vue.use(VueRouter)
Vue.use(vuetify)
Vue.config.productionTip = false

const router = new VueRouter({
  routes: Routes
})
new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
