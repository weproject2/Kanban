from flask_security import UserMixin, RoleMixin
from app import db
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy import PickleType


roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('role.id')))

lists_users = db.Table('lists_users',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('list_id', db.Integer, db.ForeignKey('list.list_id')))

cards_lists = db.Table('cards_lists',
    db.Column('list_id', db.Integer, db.ForeignKey('list.list_id')),
    db.Column('card_id', db.Integer, db.ForeignKey('card.card_id')))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime)
    roles = db.relationship(
        'Role', 
        secondary=roles_users, 
        backref=db.backref('users', lazy='dynamic')
    )
    lists = db.relationship(
        'List', 
        secondary=lists_users, 
        backref=db.backref('users', lazy='dynamic')
    )

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    description = db.Column(db.String(255))

class List(db.Model):
    list_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    lname = db.Column(db.String(70))
    cards = db.relationship(
        'Card', 
        secondary=cards_lists, 
        backref=db.backref('lists', lazy='dynamic')
    )
    def __init__(self, user_id, lname):
        self.user_id = user_id
        self.lname = lname

class Card(db.Model):
    card_id = db.Column(db.Integer, primary_key=True)
    lid = db.Column(db.Integer, db.ForeignKey('list.list_id'))
    title = db.Column(db.String(70))
    content = db.Column(db.String(300))
    deadline = db.Column(db.String)
    completed = db.Column(db.String)
    completed_time = db.Column(db.String)
    start_time = db.Column(db.String)
    last_modified = db.Column(db.String)
    started_in = db.Column(db.Integer, db.ForeignKey('list.list_id'))
    card_journey = db.Column(MutableList.as_mutable(PickleType),
                                    default=[])
    def __init__(self, lid, title, content, deadline, completed, completed_time, start_time, last_modified, started_in):
        self.lid = lid
        self.title = title
        self.content = content
        self.deadline = deadline
        self.completed = completed
        self.completed_time = completed_time
        self.start_time = start_time
        self.last_modified = last_modified
        self.started_in = started_in
        
    