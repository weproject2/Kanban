from functools import wraps
from app import db, app
from flask import request, jsonify, make_response, send_from_directory
from flask_security.utils import hash_password, verify_password
from flask_security import login_required
from flask_restful import Resource, Api
from matplotlib import pyplot as plt, dates
from models import User, List, Card
import models
import datetime
import jwt
import base64



def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'x_access_token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
            return jsonify({'message': 'a valid token is missing'})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            print(data)
        except:
            print('LAlLALALALALA')
            return jsonify({'message': 'token is invalid'})
        return f(*args, **kwargs)
    return decorator


# User CRUD
class UserRegister(Resource):
    def get(self):
        return jsonify(message="Please Register")
    def post(self):  
        data = request.get_json()  
        if models.User.query.filter_by(email=data['email']).first():
            return make_response('Error! User with given email already exists. Try a different email or login.', 403, {'WWW.Authentication': 'Basic realm: "user already exists"'})  
        hashed_password = hash_password(data['password'])
        new_user = User(email=data['email'], password=hashed_password) 
        db.session.add(new_user)  
        db.session.commit()    
        return jsonify({'message': 'registered successfully', 'Access-Control-Allow-Origin': '*'})


class UserLogin(Resource):
    @token_required
    def get(self, email):
        x = models.User.query.filter_by(email=email).first()
        return jsonify({'id': x.id,
        'email': x.email,
        'password': x.password})
    def post(self): 
        data = request.get_json()
        user = User.query.filter_by(email=data['email']).first()
        if not data:
            return make_response('Error! Login to access the page.', 401, {'WWW.Authentication': 'Basic realm: "login required"'})  
        if not user:
            return make_response('Error! User does not exist. Enter correct email or sign up.', 404, {'WWW.Authentication': 'Basic realm: "user does not exist"'}) 
        if verify_password(data['password'], user.password):  
            token = jwt.encode({'id': user.id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])  
            return jsonify({'token' : token.decode("utf-8")}) 
        return make_response('Error! The password you entered is incorrect',  401, {'WWW.Authentication': 'Basic realm: "login required"'})
    @token_required
    def put(self, email):
        data = request.get_json()
        user = User.query.filter_by(email=email).first()
        user.email = data['email']
        user.password = hash_password(data['password'])
        db.session.commit()
        return jsonify({'message': 'user info updated successfully'})
    @token_required
    def delete(self, email):
        current_user = User.query.filter_by(email=email).first()
        user_id = current_user.id
        [models.Card.query.filter_by(lid=x.lid).delete() for x in models.List.query.filter_by(user_id=user_id).all()]
        models.List.query.filter_by(user_id=user_id).delete()
        User.query.filter_by(id=user_id).delete()
        db.session.commit()
        return jsonify({'message': 'user deleted successfully'})

# class Board(Resource):
#     show all lists
# List CRUD
class List(Resource):
    @token_required
    def get(self, email):
        print('lol')
        user = User.query.filter_by(email=email).first()
        lists = models.List.query.filter_by(user_id=user.id).all()
        print('ATTENTION!')
        print([{'list_id': l.list_id,
        'user_id': l.user_id,
        'lname': l.lname,
        'cards': [{'lid': c.lid,
            'title': c.title,
                    'content': c.content,
                    'deadline': c.deadline,
                    'completed_time': c.completed_time,
                    'completed': c.completed,
                    'start_time': c.start_time,
                    'last_modified': c.last_modified,
                    'started_in': c.started_in,
                    'card_journey': [{'change_list_id': x[0],
                    'change_timestamp': x[1]} for x in c.card_journey]} for c in models.Card.query.filter_by(lid=l.list_id).all()]} for l in lists])
        return jsonify({'lists': [{'list_id': l.list_id,
        'user_id': l.user_id,
        'lname': l.lname,
        'cards': [{'lid': c.lid,
        'card_id': c.card_id,
            'title': c.title,
                    'content': c.content,
                    'deadline': c.deadline,
                    'completed_time': c.completed_time,
                    'completed': c.completed,
                    'start_time': c.start_time,
                    'last_modified': c.last_modified,
                    'started_in': c.started_in,
                    'card_journey': c.card_journey} for c in models.Card.query.filter_by(lid=l.list_id).all()]} for l in lists]})
    @token_required
    def post(self):
        data = request.get_json()
        current_user = User.query.filter_by(email=data['email']).first()
        new_list = models.List(current_user.id, data['lname']) 
        db.session.add(new_list)  
        db.session.commit()    
        return jsonify({'message': 'list added successfully', 'Access-Control-Allow-Origin': '*'})
    @token_required
    def put(self, email):
        data = request.get_json()
        list = models.List.query.filter_by(list_id=data['list_id']).first()
        list.lname = data['lname']
        db.session.commit()
        return jsonify({'message': 'list info updated successfully', 'Access-Control-Allow-Origin': '*'})
    @token_required
    def delete(self, list_id):
        models.Card.query.filter_by(lid=list_id).delete()
        models.List.query.filter_by(list_id=list_id).delete()
        db.session.commit()
        return jsonify({'message': 'list deleted successfully', 'Access-Control-Allow-Origin': '*'})

# Card CRUD
class Card(Resource):
    @token_required
    def get(self, lid):
        c = models.Card.query.filter_by(lid=lid).all()
        return jsonify({'cards': [{'card_id': y.card_id,
        'lid': y.lid,
        'title': y.title,
        'content': y.content,
        'deadline': y.deadline,
        'completed': y.completed,
        'completed_time': y.completed_time,
        'start_time': y.start_time,
        'last_modified': y.last_modified,
        'started_in': y.started_in,
        'card_journey': y.card_journey} for y in c]})
    @token_required
    def post(self):
        data = request.get_json()
        new_card = models.Card(lid=data['lid'], title=data['title'], content=data['content'], deadline=data['deadline'], completed=data['completed'], completed_time=data['completed_time'], start_time=data['start_time'], last_modified=data['last_modified'], started_in=data['lid']) 
        db.session.add(new_card)  
        db.session.commit()    
        return jsonify({'message': 'card added successfully'})
    @token_required
    def put(self, card_id):
        data = request.get_json()
        card = models.Card.query.filter_by(card_id=card_id).first()
        prev_lid = card.lid
        card.lid = data['lid']
        card.title = data['title']
        card.content = data['content']
        card.deadline = data['deadline']
        card.completed = data['completed']
        card.completed_time = data['completed_time']
        card.start_time = data['start_time']
        card.last_modified = data['last_modified']
        if (prev_lid != card.lid):
            card.card_journey = card.card_journey + [(card.lid, card.last_modified)]
        db.session.commit()
        return jsonify({'message': 'card info updated successfully', 'Access-Control-Allow-Origin': '*'})
    @token_required
    def delete(self, card_id):
        models.Card.query.filter_by(card_id=card_id).delete()
        db.session.commit()
        return jsonify({'message': 'card deleted successfully', 'Access-Control-Allow-Origin': '*'})

class Graph(Resource):
    @token_required
    def get(self, email):
        print('GRAPHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
        user = User.query.filter_by(email=email).first()
        lists = models.List.query.filter_by(user_id=user.id).all()
        lnames = {}
        for l in lists:
            lnames[l.list_id] = l.lname
        i = 1
        for l in lists:
            gname = 'graph' + str(i) + '.png'
            for c in models.Card.query.filter_by(lid=l.list_id).all():
                x = [c.start_time] + [x[1] for x in c.card_journey]
                y = [lnames[c.started_in]] + [lnames[x[0]] for x in c.card_journey]
                if (c.completed == 'True'):
                    x += [c.completed_time]
                    y += ['Completed']
                print(x, y, len(x))
                if (len(x) >= 2):
                    plt.plot(x, y)
                    plt.tight_layout()
                    plt.savefig('../img/'+gname)
                    print(gname)
            plt.close()
            i += 1
        return jsonify({'message': 'Graph has been updated'})

class ServeGraph(Resource):
    def get(self, filename):
        encoded = base64.b64encode(open("../img/" + filename, "rb").read())
        new_encode = str(encoded)[2:-3]
        return jsonify({"base64": str(new_encode),"base64_1": None,"base64_2": None})



# Views
# @app.route('/')
# @login_required
# def home():
#     return render_template('index.html')