from email import encoders
from email.mime.base import MIMEBase
from flask import Flask, render_template, request, redirect, url_for, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, login_required, UserMixin, RoleMixin
from flask_security.utils import hash_password, verify_password
from flask_restful import Api
import os
import jwt
from functools import wraps
import datetime
from flask_cors import CORS
import csv

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

dir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY'] = 'thisisasecret'
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + os.path.join(dir, ("db" + ".sqlite3"))
app.config['SECURITY_PASSWORD_SALT'] = 'thisisasecretsalt'
app.app_context().push()
db = SQLAlchemy(app)
api = Api(app)
from models import User, Role, List as mList, Card as mCard
from apis import UserLogin, UserRegister, Card, List, Graph, ServeGraph


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
db.create_all()


api.add_resource(UserLogin, '/userlogin', '/userlogin/<string:email>')
api.add_resource(UserRegister, '/userregister')
api.add_resource(List, '/list', '/list/<string:email>', '/list/<int:list_id>')
api.add_resource(Card, '/card', '/card/<int:card_id>')
api.add_resource(Graph, '/graph/<string:email>')
api.add_resource(ServeGraph, '/images/<string:filename>')

SMPTP_SERVER_HOST = "localhost"
SMPTP_SERVER_PORT = 1025
SENDER_ADDRESS = "osmanimadhavi@gmail.com"
SENDER_PASSWORD = ""

@app.route('/exportlist', methods=['POST'])
def export_list():
    data = request.get_json() 
    to_address = data['email']
    subject = data['subject']
    message = data['message']
    lid = data['lid']
    list = mList.query.filter_by(list_id=lid).first()
    path = "static/" + list.lname + ".csv"
    rows = []
    rows.append([list.lname + ' contents:'])
    rows.append(['Title', 'Content', 'Completed?', 'Deadline'])
    for c in mCard.query.filter_by(lid=lid).all():
        rows.append([c.title, c.content, c.completed, c.deadline])
    with open(path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(rows)
    msg = MIMEMultipart()
    msg["From"] = SENDER_ADDRESS
    msg["To"] = to_address
    msg["Subject"] = subject
    msg.attach(MIMEText(message, "html"))
    filename = list.lname + ".csv"
    with open(path,'rb') as file:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(file.read())
    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition", f"attachment; filename = {filename}"
    )
    msg.attach(part)
    s = smtplib.SMTP(host=SMPTP_SERVER_HOST, port=SMPTP_SERVER_PORT)
    s.login(SENDER_ADDRESS, SENDER_PASSWORD)
    s.send_message(msg)
    s.quit()
    return jsonify({'message': 'Exported list', 'Access-Control-Allow-Origin': '*'})

@app.route('/exportcard', methods=['POST'])
def export_card():
    data = request.get_json() 
    to_address = data['email']
    subject = data['subject']
    message = data['message']
    cid = data['cid']
    card = mCard.query.filter_by(card_id=cid).first()
    path = "static/" + card.title + ".csv"
    rows = []
    rows.append([card.title + ' contents:'])
    rows.append(['Title', 'Content', 'Completed?', 'Deadline'])
    rows.append([card.title, card.content, card.completed, card.deadline])
    with open(path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(rows)
    msg = MIMEMultipart()
    msg["From"] = SENDER_ADDRESS
    msg["To"] = to_address
    msg["Subject"] = subject
    msg.attach(MIMEText(message, "html"))
    filename = card.title + ".csv"
    with open(path,'rb') as file:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(file.read())
    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition", f"attachment; filename = {filename}"
    )
    msg.attach(part)
    s = smtplib.SMTP(host=SMPTP_SERVER_HOST, port=SMPTP_SERVER_PORT)
    s.login(SENDER_ADDRESS, SENDER_PASSWORD)
    s.send_message(msg)
    s.quit()
    return jsonify({'message': 'Exported list', 'Access-Control-Allow-Origin': '*'})


if __name__ == '__main__':
    app.run()